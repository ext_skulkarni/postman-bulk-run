## This repo enables running Postman collections as CLI programs using the newman tool.  It's very handy for stress/bulk API testing.

### Steps to run:
1.  npm install
2.  Export from Postman and add the collection and environment files you want to run as JSON files in project
3.  Add complimenting CSV file as data
4.  Run `npm start`
5.  Sit back and watch the magic

### Random Data:
Random data can be generated from following:

```
function random(length, onlyNumbers = false){
    let chars = onlyNumbers ?  '0123456789' : 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789012345678901234567890123456789';
    let str = '';
    for (let i = 0; i < length; i++) {
        str += chars.charAt(Math.floor(Math.random() * chars.length));
    }

    return str;
};

postman.setEnvironmentVariable("name", random(12));
postman.setEnvironmentVariable("description", random(50));
postman.setEnvironmentVariable("id", random(8));
postman.setEnvironmentVariable("barcode", random(10, true));
```

Or to generate a CSV format, use the following.  It will generate 1000 lines of comma separated values using the random function above.

```
[...Array(1000).keys()].forEach(r => {
    const i = `${random(7)},${random(10)},${random(10)+"  "+random(12)},${random(10,true)}`;
    console.log(i)
});
```
